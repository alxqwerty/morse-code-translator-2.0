﻿using System;
using System.Text;

#pragma warning disable S2368

namespace MorseCodeTranslator
{
    public static class Translator
    {
        public static string TranslateToMorse(string message)
        {
            if (message is null)
            {
                throw new ArgumentNullException(nameof(message), "Message cannot be null.");
            }

            StringBuilder morseMessageBuilder = new StringBuilder();

            for (int i = 0; i < message.Length; i++)
            {
                int symbolIndex = FindIndexOfChar(message, i);

                if (symbolIndex == 27)
                {
                    continue;
                }

                for (int j = 1; j < MorseCodes.CodeTable[symbolIndex].Length; j++)
                {
                    morseMessageBuilder.Append(MorseCodes.CodeTable[symbolIndex][j]);
                }

                if (i < message.Length - 1)
                {
                    if (message[i + 1] == '.' && i == message.Length - 2)
                    {
                        break;
                    }
                    else
                    {
                        morseMessageBuilder.Append(' ');
                    }
                }
            }

            return new string(morseMessageBuilder.ToString());
        }

        public static string TranslateToText(string morseMessage)
        {
            if (morseMessage is null)
            {
                throw new ArgumentNullException(nameof(morseMessage), "Morse message cannot be null.");
            }

            StringBuilder messageBuilder = new StringBuilder();

            string[] splittedMessage = morseMessage.Split(' ');

            for (int i = 0; i < splittedMessage.Length; i++)
            {
                int symbolIndex = FindIndexOfMorse(morseMessage, i, splittedMessage);

                if (symbolIndex == 27)
                {
                    continue;
                }

                messageBuilder.Append(MorseCodes.CodeTable[symbolIndex][0]);
            }

            return new string(messageBuilder.ToString());
        }

        public static void WriteMorse(char[][] codeTable, string message, StringBuilder morseMessageBuilder, char dot = '.', char dash = '-', char separator = ' ')
        {
            if (codeTable is null)
            {
                throw new ArgumentNullException(nameof(codeTable), "Code table cannot be null.");
            }

            if (message is null)
            {
                throw new ArgumentNullException(nameof(message), "Message cannot be null.");
            }

            if (morseMessageBuilder is null)
            {
                throw new ArgumentNullException(nameof(morseMessageBuilder), "Morse message builder cannot be null.");
            }

            if (dot == '*' && dash == '=' && separator == '.')
            {
                codeTable = ExternalMorseCode.CodeTable;
            }

            char[] convertedMessage = message.ToUpperInvariant().ToCharArray();

            for (int i = 0; i < convertedMessage.Length; i++)
            {
                for (int k = 0; k < codeTable?.Length; k++)
                {
                    if (convertedMessage[i] == codeTable?[k][0])
                    {
                        for (int j = 1; j < codeTable?[k].Length; j++)
                        {
                            morseMessageBuilder.Append(codeTable?[k][j]);
                        }

                        if (i < convertedMessage.Length - 1)
                        {
                            if ((convertedMessage[i + 1] == '.' || convertedMessage[i + 1] == '*') && i == convertedMessage.Length - 2)
                            {
                                break;
                            }
                            else
                            {
                                morseMessageBuilder.Append(separator);
                            }
                        }
                    }
                }
            }
        }

        public static void WriteText(char[][] codeTable, string morseMessage, StringBuilder messageBuilder, char dot = '.', char dash = '-', char separator = ' ')
        {
            if (codeTable is null)
            {
                throw new ArgumentNullException(nameof(codeTable), "Code table cannot be null.");
            }

            if (morseMessage is null)
            {
                throw new ArgumentNullException(nameof(morseMessage), "Morse message cannot be null.");
            }

            if (messageBuilder is null)
            {
                throw new ArgumentNullException(nameof(messageBuilder), "Message builder cannot be null.");
            }

            string[] splittedMessage = morseMessage.Split(separator);

            for (int i = 0; i < splittedMessage.Length; i++)
            {
                int symbolIndex = FindIndexOfMorse(morseMessage, i, splittedMessage, dot, dash, separator);

                if (symbolIndex == 27)
                {
                    continue;
                }

                messageBuilder.Append(MorseCodes.CodeTable[symbolIndex][0]);
            }
        }

        public static int FindIndexOfChar(string message, int currentSearchingIndex)
        {
            if (message is null)
            {
                throw new ArgumentNullException(nameof(message), "Message cannot be null.");
            }

            if (currentSearchingIndex < 0 || currentSearchingIndex > message.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(currentSearchingIndex), "Index cannot be out of range.");
            }

            char[] convertedMessage = message.ToUpperInvariant().ToCharArray();

            int resultIndex = 27;

            for (int i = 0; i < MorseCodes.CodeTable.Length; i++)
            {
                if (convertedMessage[currentSearchingIndex] == MorseCodes.CodeTable[i][0])
                {
                    resultIndex = i;
                    break;
                }
            }

            return resultIndex;
        }

        public static int FindIndexOfMorse(string morseMessage, int currentSearchingIndex, string[] splittedMessage)
        {
            if (morseMessage is null)
            {
                throw new ArgumentNullException(nameof(morseMessage), "Message cannot be null.");
            }

            if (currentSearchingIndex < 0 || currentSearchingIndex > morseMessage.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(currentSearchingIndex), "Index cannot be out of range.");
            }

            if (splittedMessage is null)
            {
                throw new ArgumentNullException(nameof(splittedMessage), "Splitted message cannot be null.");
            }

            int resultIndex = 27;

            for (int i = 0; i < MorseCodes.CodeTable.Length; i++)
            {
                if (splittedMessage[currentSearchingIndex].Length != MorseCodes.CodeTable[i].Length - 1)
                {
                    continue;
                }

                for (int j = MorseCodes.CodeTable[i].Length - 1; j > 0; j--)
                {
                    if (splittedMessage[currentSearchingIndex][j - 1] != MorseCodes.CodeTable[i][j])
                    {
                        break;
                    }
                    else if (splittedMessage[currentSearchingIndex][j - 1] == MorseCodes.CodeTable[i][j] && j == 1)
                    {
                        resultIndex = i;
                        return resultIndex;
                    }
                }
            }

            return resultIndex;
        }

        public static int FindIndexOfMorse(string morseMessage, int currentSearchingIndex, string[] splittedMessage, char dot, char dash, char separator)
        {
            if (morseMessage is null)
            {
                throw new ArgumentNullException(nameof(morseMessage), "Message cannot be null.");
            }

            if (currentSearchingIndex < 0 || currentSearchingIndex > morseMessage.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(currentSearchingIndex), "Index cannot be out of range.");
            }

            if (splittedMessage is null)
            {
                throw new ArgumentNullException(nameof(splittedMessage), "Splitted message cannot be null.");
            }

            char[][] codeTable;

            if (dot == '*' && dash == '=' && separator == '.')
            {
                codeTable = ExternalMorseCode.CodeTable;
            }
            else
            {
                codeTable = MorseCodes.CodeTable;
            }

            int resultIndex = 27;

            for (int i = 0; i < codeTable.Length; i++)
            {
                if (splittedMessage[currentSearchingIndex].Length != codeTable[i].Length - 1)
                {
                    continue;
                }

                for (int j = codeTable[i].Length - 1; j > 0; j--)
                {
                    if (splittedMessage[currentSearchingIndex][j - 1] != codeTable[i][j])
                    {
                        break;
                    }
                    else if (splittedMessage[currentSearchingIndex][j - 1] == codeTable[i][j] && j == 1)
                    {
                        resultIndex = i;
                        return resultIndex;
                    }
                }
            }

            return resultIndex;
        }
    }
}
